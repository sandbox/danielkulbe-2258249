(function($) {
/**
 * @file
 * QueryLoader2 content script.
 */

Drupal.behaviors.query_loader = {
  fadeIn:  function () {
    $(Drupal.settings.query_loader.selector).queryLoader2(Drupal.settings.query_loader);
  },
  fadeOut: function (event) {
    $('<div id="qLoverlay"></div>')
      .appendTo($(Drupal.settings.query_loader.selector))
      .css({
        position: "fixed",
        zIndex: 666999, //very high!
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: "none",
        backgroundColor: Drupal.settings.query_loader.backgroundColor,
        backgroundPosition: "fixed"
      })
      .fadeIn(Drupal.settings.query_loader.minimumTime)
    ;
  },
  attach: function(context, settings) {
    if (Drupal.settings.query_loader.adminPage) {
      Drupal.settings.query_loader.selector += ':not(.overlay)';
    }
    else {
      Drupal.settings.query_loader.selector += ':not(.page-admin,.overlay)';
    }
    $(document).on('ready', Drupal.behaviors.query_loader.fadeIn);
    $(window).on('beforeunload', Drupal.behaviors.query_loader.fadeOut);
  }
};


/**
 * Override click event handler on overlay module to fix issue with download links (onbeforeunload)
 */
Drupal.overlay = Drupal.overlay || {};

Drupal.overlay.eventhandlerOverrideLink = function (event) {
  // In some browsers the click event isn't fired for right-clicks. Use the
  // mouseup event for right-clicks and the click event for everything else.
  if ((event.type == 'click' && event.button == 2) || (event.type == 'mouseup' && event.button != 2)) {
    return;
  }

  var $target = $(event.target);

  // Only continue if clicked target (or one of its parents) is a link.
  if (!$target.is('a')) {
    $target = $target.closest('a');
    if (!$target.length) {
      return;
    }
  }

  // Never open links in the overlay that contain the overlay-exclude class.
  if ($target.hasClass('overlay-exclude')) {
    return;
  }

  // Close the overlay when the link contains the overlay-close class.
  if ($target.hasClass('overlay-close')) {
    // Clearing the overlay URL fragment will close the overlay.
    $.bbq.removeState('overlay');
    return;
  }

  var target = $target[0];
  var href = target.href;
  // Only handle links that have an href attribute and use the HTTP(S) protocol.
  if (href != undefined && href != '' && target.protocol.match(/^https?\:/)) {
    var anchor = href.replace(target.ownerDocument.location.href, '');
    // Skip anchor links.
    if (anchor.length == 0 || anchor.charAt(0) == '#') {
      return;
    }
    // Open admin links in the overlay.
    else if (this.isAdminLink(href)) {
      // If the link contains the overlay-restore class and the overlay-context
      // state is set, also update the parent window's location.
      var parentLocation = ($target.hasClass('overlay-restore') && typeof $.bbq.getState('overlay-context') == 'string')
        ? Drupal.settings.basePath + $.bbq.getState('overlay-context')
        : null;
      href = this.fragmentizeLink($target.get(0), parentLocation);
      // Only override default behavior when left-clicking and user is not
      // pressing the ALT, CTRL, META (Command key on the Macintosh keyboard)
      // or SHIFT key.
      if (event.button == 0 && !event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
        // Redirect to a fragmentized href. This will trigger a hashchange event.
        this.redirect(href);
        // Prevent default action and further propagation of the event.
        return false;
      }
      // Otherwise alter clicked link's href. This is being picked up by
      // the default action handler.
      else {
        $target
          // Restore link's href attribute on blur or next click.
          .one('blur mousedown', { target: target, href: target.href }, function (event) { $(event.data.target).attr('href', event.data.href); })
          .attr('href', href);
      }
    }
    // Non-admin links should close the overlay and open in the main window,
    // which is the default action for a link. We only need to handle them
    // if the overlay is open and the clicked link is inside the overlay iframe.
    else if (this.isOpen && target.ownerDocument === this.iframeWindow.document) {
      // Open external links in the immediate parent of the frame, unless the
      // link already has a different target.
      var _target  = '_parent';

      // Open files in external window to prevent 'beforeunload' Event
      if($target.parent().hasClass('update-download') || undefined != $target.attr('download')) {
        // fallback for old browsers
        _target = 'download';

        // use HTML5 download attribute
        var filename = target.href.split('/');
        filename = filename[filename.length-1];
        $target.attr('download', filename);
      }

      if (target.hostname != window.location.hostname) {
        if (!$target.attr('target')) {
          $target.attr('target', _target);
        }
      }
      else {
        // Add the overlay-context state to the link, so "overlay-restore" links
        // can restore the context.
        if ($target[0].hash) {
          // Leave links with an existing fragment alone. Adding an extra
          // parameter to a link like "node/1#section-1" breaks the link.
        }
        else {
          // For links with no existing fragment, add the overlay context.
          $target.attr('href', $.param.fragment(href, { 'overlay-context': this.getPath(window.location) + window.location.search }));
        }

        // When the link has a destination query parameter and that destination
        // is an admin link we need to fragmentize it. This will make it reopen
        // in the overlay.
        var params = $.deparam.querystring(href);
        if (params.destination && this.isAdminLink(params.destination)) {
          var fragmentizedDestination = $.param.fragment(this.getPath(window.location), { overlay: params.destination });
          $target.attr('href', $.param.querystring(href, { destination: fragmentizedDestination }));
        }

        // Make the link open in the immediate parent of the frame, unless the
        // link already has a different target.
        if (!$target.attr('target')) {
          $target.attr('target', _target);
        }
      }
    }
  }
};

})(jQuery);