  __   _  _  ____  ____  _  _  __     __    __   ____  ____  ____  ____
 /  \ / )( \(  __)(  _ \( \/ )(  )   /  \  / _\ (    \(  __)(  _ \(___ \
(  O )) \/ ( ) _)  )   / )  / / (_/\(  O )/    \ ) D ( ) _)  )   / / __/
 \__\)\____/(____)(__\_)(__/  \____/ \__/ \_/\_/(____/(____)(__\_)(____)

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * QueryLoader2
 * Installation
 * Credits


INTRODUCTION
------------

This module implements QueryLoader2 into Drupal and plovides additionally
a page unload animation.


QUERYLOADER2
------------
QueryLoader v2 is a better version of the old script posted in 2009. It serves
the main purpose of preloading the images on your website by showing an overlay
and a loading bar. It automatically fetches all your images and background
images and preloads them before showing the webpage. QueryLoader currently
works in IE version 9+, Chrome, Safari and Firefox.
(source: https://github.com/Gaya/QueryLoader2)


INSTALLATION
------------

1. Copy the query_loader directory to your sites/SITENAME/modules directory.

2. Download the QueryLoader2 library from https://github.com/Gaya/QueryLoader2
   (minimum required version 3.0.3)

3. Extract QueryLoader2 into your sites/SITENAME/libraries directory, rename
   directory to query_loader.

4. Enable the module at Administer >> Site building >> Modules.


CREDITS
-------
* Daniel Kulbe (dku)
